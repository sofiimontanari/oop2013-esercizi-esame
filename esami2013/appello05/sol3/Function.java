package esami2013.appello05.sol3;

public interface Function<I,O> {
	O apply(I i);
}
